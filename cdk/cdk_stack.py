from constructs import Construct

from aws_cdk import (
    Duration,
    Stack,
    aws_s3 as s3
)


class CdkStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        bucket = s3.Bucket(self , 'my-test-se-bucket-cdk')
        bucket.add_lifecycle_rule(
        transitions=[
            s3.Transition(
            storage_class=s3.StorageClass.GLACIER,
            transition_after=Duration.days(10)
            )
        ]
        )

